=== PROCEDURAL ISLAND GENERATION ==

Important to note:

	- This tool does not require any dependancies outside of the default Unity packages, but it is important to note that it was created in Unity version 2021.3.4f1.

Use:

	- This tool is designed to use within prototype projects needing procedurally generated islands.

	- For ease of use, the 4 game objects in the "Example Scene" (excluding the camera and directional light), can simply be copied and pasted into a new scene, ready to be used.

	- As demonstrated in the "Example Scene", for this tool to work, the scene requires 2 game objects (1 empty and 1 terrain) and 2 optional game objects (1 quad for the noise texture display and 1 plane for the water).

		- The empty game object should have the "Noise Generator" script attatched to it.

		- The terrain should have the "Paint Terrain" script attatched to it.

Noise Generator Script:

	- Seed

		- The seed is what is used to generate the terrain consistantly. This variable is public, so it is easily accessible from other scripts ("GetComponent<NoiseGenerator>().seed" for example).

	- Offset X and Offset Y

		- These offsets can be used to either slightly tweak the position of the generated terrain heights or to consistantly tile terrains using the same seed.

	- Terrain Object

		- Here, the required terrain object must be referenced (this is the only REQUIRED reference).

	- Noise Display Object

		- This variable is optional. If there is a quad that should display the generated noise texture, this is where it should be referenced.

	- Depth and Flatness

		- Depth and Flatness do very similar things. Both variables influence the height of the mountains and hills on the island.

		- Depth simply exagurates the heights of these hills and mountains.

		- Flatness flattens out the island for a smoother transition between valleys and mountains.

	- Scale

		- This is not the scale of the island, this is the scale of the noise texture used to generate the island. Changing this value will increase or decrease the size and number of hills and mountains.

	- Falloff Range

		- The fallloff range can be seen as the size of the island. What it does, is it determines how far from the centre of the island, the island should generate before transitioning into the sea.

	- Falloff Power

		- This variable determines how harshly the island transitions from beach to mountain. Higher values will cause mountains to generate closer to beaches and force the beaches to be smaller, whereas lower values have the opposite effect.

Paint Terrain Script:

	- Important Note

		- This script is reliant on the terrain layers of the terrain. Add layers to the terrain as you normally would, sorting the textures from bottom to top.

	- Splat Heights

		- Here, the desired terrain textures are set up by height. (The first texture in the terrain's texture layers is in index 0, not index 1).
		
		- Add elements based on the amount of textures there are in the terrain layers.

		- Texture Index

			- Here, the index of the texture wanted is added (for example, if the 1st texture in the texture layers  is to be used, the Texture Index will be 0).

		- Starting Height

			- This is where the starting height of the textures are set. Higher values here, will cause the textures to be painted higher on the terrain (Element 0's Starting Height should always be 0).

	- Map Generator Object

		- This is simply a reference to the game object with the Noise Generator script.

		- This is not needed to be referenced as long as the terrain itself is referenced in the Noise Generator script.

		- If something else is referenced here, it will automatically correct itself.

If anything within this README is unclear, have a look at the example within the "Example Scene".